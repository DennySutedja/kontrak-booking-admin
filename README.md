## GET ALL BOOKING

req : https://api-dev/booking?page=1&limit=10&search=str&status=&building_id=1
status : ["all","booked","check-in","check-out","canceled"]

```json
{
    "page" : 1,
    "limit": 2,
    "search": "str",
    "status":"",
    "building_id":1
}
```

response : 

```json
{
"data" : {
    "items": [
        {
            "booking_id":"1",
            "building_name" :"MTHR",
            "resident_name":"dadang jordan",
            "booking_number": "Y1231175576",
            "facility_name":"gym",
            "status":"booked",
            "created_date":"2022-10-18 13:00:00"
        },
        {
            "booking_id":"2",
            "building_name" :"Eastpark",
            "resident_name":"endang beck",
            "booking_number": "P1231175576",
            "facility_name":"pool",
            "status":"check-in",
            "created_date":"2022-10-18 14:00:00"
        },
    ],
    "total_pages":2,
    "total_items":4
    }
}

```

## GET Booking Detail
req : https://api-dev/booking/10

response:

```json
{
    "data":{
        "booking_id":1,
        "booking_number":"P1231175576",
        "created_date":"2022-10-18 14:00:00",
        "resident_name":"endang beck",
        "tower":"Wings",
        "unit":"AB/02/10",
        "phone":"6285612345678",
        "email":"endang@beck.com",
        "schedule_start":"2022-10-19 14:00:00",
        "schedule_end":"2022-10-19 15:00:00"
    }
}
```
 
## GET LIST FACILITY

req : https://api-dev/booking?page=1&limit=10&search=str&status=&building_id=1
status : ["all","open","closed"]

```json
{
    "page" : 1,
    "limit": 2,
    "building_id":1
}
```

response

```json
{
"data" : {
    "items": [
        {
            "facility_id":"1",
            "building_name" :"MTHR",
            "facility_name":"gym",
            "status":"open",
            "total_quota_per_duration":100,
            "used_quota_per_duration":10
        },
        {
            "facility_id":"1",
            "building_name" :"MTHR",
            "facility_name":"gym",
            "status":"open",
            "total_quota_per_duration":100,
            "used_quota_per_duration":10
        },
    ],
    "total_pages":2,
    "total_items":4
    }
}
```

## FACILITY DETAIL

req : https://api-dev/booking/10

response : 

```json
    {
        "thumbnail_url":"url_thumbnail",
        "image_urls":[
            {
                "id":"1028123",
                "url":"image1"
            },
            {
                "id":"1028122",
                "url":"image2"
            }
        ],
        "id":"2",
        "name":"pool",
        "description":"Gold's Gym Indonesia .......",
        "building_id":1,
        "building_name" :"Eastpark",
        "location": "loc",
        "status":"closed",
        "open_time": "01:00",
        "close_time": "23:59"
        "created_date":"2022-10-18 14:00:00",
        "check_in_start_minute":15,
        "other_facilities": [
            {
                "id":"fac123123",
                "name":"Toilet",
                "images":"url1"
            },
            {
                "id":"fac123124",
                "name":"Wifi",
                "images":"url2"
            },
        ],
        "rules":[
            {
                "id":"rule1",
                "description":"desc"
            },
            {
                "id":"rule2",
                "description":"desc2"
            },
        ],
        "open_schedules": [
            {
                "id":"idsche1",
                "day": "Sunday",
                "open_time": "00:00:01",
                "close_time": "23:59:59",
                "quota_per_duration": 100,
                "duration": 1
            },
            {
                "id":"idsche2",
                "day": "Monday",
                "open_time": "00:00:01",
                "close_time": "23:59:59",
                "quota_per_duration": 100,
                "duration": 1
            },
            {
                "id":"idsche3",
                "day": "Tuesday",
                "open_time": "00:00:01",
                "close_time": "23:59:59",
                "quota_per_duration": 100,
                "duration": 1
            },
            {
                "id":"idsche4",
                "day": "Wednesday",
                "open_time": "00:00:01",
                "close_time": "23:59:59",
                "quota_per_duration": 100,
                "duration": 1
            },
            {
                "id":"idsche5",
                "day": "Thursday",
                "open_time": "00:00:01",
                "close_time": "23:59:59",
                "quota_per_duration": 100,
                "duration": 1
            },
            {
                "id":"idsche6",
                "day": "Friday",
                "open_time": "00:00:01",
                "close_time": "23:59:59",
                "quota_per_duration": 100,
                "duration": 1
            },
            {
                "id":"idsche7",
                "day": "Saturday",
                "open_time": "00:00:01",
                "close_time": "23:59:59",
                "quota_per_duration": 100,
                "duration": 1
            }
        ]
    }

```

## CREATE FACILITY

body :

```json
{
    "building_id":1,
    "location":"loc",
    "thumbnail_url":"url",
    "image_urls":["image1","image2"],
    "name":"gym",
    "description":"ini adalah deskripsi",
    "rules":["rule1","rule2"],
    "other_facilities":["wifi","ac"],
    "check_in_start_minute":15,
    "open_schedules": [
            {
                "day": "Sunday",
                "open_time": "00:00:01",
                "close_time": "23:59:59",
                "quota_per_duration": 100,
                "duration": 1
            },
            {
                "day": "Monday",
                "open_time": "00:00:01",
                "close_time": "23:59:59",
                "quota_per_duration": 100,
                "duration": 1
            },
            {
                "day": "Tuesday",
                "open_time": "00:00:01",
                "close_time": "23:59:59",
                "quota_per_duration": 100,
                "duration": 1
            },
            {
                "day": "Wednesday",
                "open_time": "00:00:01",
                "close_time": "23:59:59",
                "quota_per_duration": 100,
                "duration": 1
            },
            {
                "day": "Thursday",
                "open_time": "00:00:01",
                "close_time": "23:59:59",
                "quota_per_duration": 100,
                "duration": 1
            },
            {
                "day": "Friday",
                "open_time": "00:00:01",
                "close_time": "23:59:59",
                "quota_per_duration": 100,
                "duration": 1
            },
            {
                "day": "Saturday",
                "open_time": "00:00:01",
                "close_time": "23:59:59",
                "quota_per_duration": 100,
                "duration": 1
            }
        ]
}
```

response:

```json
{
    "status":"ok",
    "facility_id":"1"
}
```


## EDIT FACILITY
body :

```json
{
    "id":"1",
    "thumbnail_url":"image",
    "image_urls":["image1","image2"],
    "name":"gym",
    "description":"ini adalah deskripsi",
    "location":"loc",
    "check_in_start_minute":15,
    "rules":[
        {
            "id":"rule1",
            "description":"desc"
        },
        {
            "id":"rule2",
            "description":"desc2"
        },
    ],
    "other_facilities": [
        {
            "id":"fac123123",
            "name":"Toilet",
            "images":"url1"
        },
        {
            "id":"fac123124",
            "name":"Wifi",
            "images":"url2"
        },
    ],
    "open_schedules": [
        {
            "id":"idsche1",
            "day": "Sunday",
            "open_time": "00:00:01",
            "close_time": "23:59:59",
            "quota_per_duration": 100,
            "duration": 1
        },
        {
            "id":"idsche2",
            "day": "Monday",
            "open_time": "00:00:01",
            "close_time": "23:59:59",
            "quota_per_duration": 100,
            "duration": 1
        },
        {
            "id":"idsche3",
            "day": "Tuesday",
            "open_time": "00:00:01",
            "close_time": "23:59:59",
            "quota_per_duration": 100,
            "duration": 1
        },
        {
            "id":"idsche4",
            "day": "Wednesday",
            "open_time": "00:00:01",
            "close_time": "23:59:59",
            "quota_per_duration": 100,
            "duration": 1
        },
        {
            "id":"idsche5",
            "day": "Thursday",
            "open_time": "00:00:01",
            "close_time": "23:59:59",
            "quota_per_duration": 100,
            "duration": 1
        },
        {
            "id":"idsche6",
            "day": "Friday",
            "open_time": "00:00:01",
            "close_time": "23:59:59",
            "quota_per_duration": 100,
            "duration": 1
        },
        {
            "id":"idsche7",
            "day": "Saturday",
            "open_time": "00:00:01",
            "close_time": "23:59:59",
            "quota_per_duration": 100,
            "duration": 1
        }
    ]
}
```
response : 

```json
{
    "status":"ok",
    "facility_id":"1"
}
```


## DELETE FACILITY
req : https://api-dev/booking/10

response : 

```json
{
    "status":"ok",
}
```